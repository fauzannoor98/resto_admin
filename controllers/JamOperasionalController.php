<?php

namespace app\controllers;

use Yii;
use app\models\JamOperasional;
use app\models\JamOperasionalSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use app\components\Helper;
use kartik\mpdf\Pdf;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;

/**
 * JamOperasionalController implements the CRUD actions for JamOperasional model.
 */
class JamOperasionalController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [  
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index','view','create','update','delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all JamOperasional models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new JamOperasionalSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single JamOperasional model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new JamOperasional model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id_operasional)
    {
        $model = new JamOperasional(['id_operasional' => $id_operasional]);

        $referrer = Yii::$app->request->referrer;

        if ($model->load(Yii::$app->request->post())) {
            $referrer = $_POST['referrer'];

            if ($model->save()) {
                Yii::$app->session->setFlash('success','Data berhasil disimpan.');
                return $this->redirect($referrer);
            }

            Yii::$app->session->setFlash('error','Data gagal disimpan. Silahkan periksa kembali isian Anda.');
        }

        return $this->render('create', [
            'model' => $model,
            'referrer'=>$referrer
        ]);

    }

    /**
     * Updates an existing JamOperasional model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $referrer = Yii::$app->request->referrer;

        if ($model->load(Yii::$app->request->post())) {
            $referrer = $_POST['referrer'];

            if ($model->save()) {
                Yii::$app->session->setFlash('success','Data berhasil disimpan.');
                return $this->redirect($referrer);
            }

            Yii::$app->session->setFlash('error','Data gagal disimpan. Silahkan periksa kembali isian Anda.');
        }

        return $this->render('update', [
            'model' => $model,
            'referrer'=>$referrer
        ]);

    }

    /**
     * Deletes an existing JamOperasional model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if ($model->delete()) {
            Yii::$app->session->setFlash('success','Data berhasil dihapus');
        } else {
            Yii::$app->session->setFlash('error','Data gagal dihapus');
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Finds the JamOperasional model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return JamOperasional the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = JamOperasional::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function exportExcel($params)
    {
    $spreadsheet = new Spreadsheet();

    $spreadsheet->setActiveSheetIndex(0);

    $sheet = $spreadsheet->getActiveSheet();

    $setBorderArray = array(
    'borders' => array(
    'allBorders' => array(
    'borderStyle' => Border::BORDER_THIN,
    'color' => array('argb' => '000000'),
    ),
    ),
    );


    $sheet->getColumnDimension('A')->setWidth(10);
            $sheet->getColumnDimension('B')->setWidth(20);
                $sheet->getColumnDimension('C')->setWidth(20);
                $sheet->getColumnDimension('D')->setWidth(20);
        
    $sheet->setCellValue('A3', 'No');
            $sheet->setCellValue('B3', 'Id Operasional');
                $sheet->setCellValue('C3', 'Jam Buka');
                $sheet->setCellValue('D3', 'Jam Tutup');
        
    $sheet->setCellValue('A1', 'Data JamOperasional');

    $sheet->mergeCells('A1:D1');

    $sheet->getStyle('A1:D3')->getFont()->setBold(true);
    $sheet->getStyle('A1:D3')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);

    $row = 3;
    $i=1;

    $searchModel = new JamOperasionalSearch();

    foreach($searchModel->getQuerySearch($params)->all() as $data){
    $row++;
    $sheet->setCellValue('A' . $row, $i);
            $sheet->setCellValue('B' . $row, $data->id_operasional);
                $sheet->setCellValue('C' . $row, $data->jam_buka);
                $sheet->setCellValue('D' . $row, $data->jam_tutup);
        
    $i++;
    }

    $sheet->getStyle('A3:D' . $row)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
    $sheet->getStyle('D3:D' . $row)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
    $sheet->getStyle('E3:D' . $row)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);


    $sheet->getStyle('C' . $row)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);

    $sheet->getStyle('A3:D' . $row)->applyFromArray($setBorderArray);

    $filename = time() . '_Data-Excel.xlsx';
    $path = 'exports/' . $filename;
    $writer = new Xlsx($spreadsheet);
    $writer->save($path);

    return $this->redirect($path);
    }

}
