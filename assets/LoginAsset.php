<?php
/**
 * Created by PhpStorm.
 * User: Fauzan
 * Date: 17/11/2019
 * Time: 16.15
 */

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class LoginAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/login.css',
    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD,
    ];

}
