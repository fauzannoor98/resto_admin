<?php

namespace app\migrations;

use yii\db\Migration;

/**
 * Class M180713025004Base
 */
class M180713025004Base extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "M180713025004Base cannot be reverted.\n";

        return false;
    }

    
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->createTable('user',[
            'id' => $this->primaryKey(),
            'username' => $this->string(255)->notNull(),
            'password' => $this->string(255)->notNull(),
            'waktu_login' => $this->dateTime()->notNull(),
            'auth_key' => $this->string(255),
        ]);

        $this->createTable('operasional',[
            'id' => $this->primaryKey(),
            'id_hari' => $this->integer(11)->notNull(),
            'tanggal' => $this->dateTime()->notNull(),
        ]);

        $this->createTable('jam_operasional',[
            'id' => $this->primaryKey(),
            'id_operasional' => $this->integer(11)->notNull(),
            'jam_buka' => $this->string(255),
            'jam_tutup' => $this->string(255),
        ]);
    }

    public function down()
    {
        echo "M180713025004Base cannot be reverted.\n";

        return false;
    }
    
}
