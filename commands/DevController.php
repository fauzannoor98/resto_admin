<?php

namespace app\commands;

use app\components\Helper;
use app\models\Departemen;
use app\models\Pegawai;
use app\models\PegawaiRekapPayroll;
use app\models\PegawaiShiftKerja;
use app\models\ShiftKerja;
use app\modules\iclock\models\Devcmds;
use app\modules\iclock\models\Iclock;
use app\modules\iclock\models\Template;
use app\modules\iclock\models\Userinfo;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Yii;
use app\models\User;
use yii\console\Controller;
use yii\helpers\Console;
use yii2mod\query\ArrayQuery;

class DevController extends Controller
{

    public function actionSetPegawaiShift()
    {
        $fileDir = 'files/import-shift-kerja.xlsx';
        if (!file_exists($fileDir)) {
            $this->stdout('File Tidak Ditemukan'.PHP_EOL, Console::FG_RED);
        }

        $reader = IOFactory::load($fileDir);
        $reader->setActiveSheetIndex(0);
        $sheet = $reader->getActiveSheet();

        $no = 1;
        for ($i = 1; $i <= 65; $i++) {
            $nama_departemen = trim($sheet->getCell("A$i")->getValue());
            $id_shift_kerja = trim($sheet->getCell("B$i")->getValue());

            $departemen = Departemen::findOne(['nama' => $nama_departemen]);
            if ($departemen !== null) {
                foreach ($departemen->manyPegawai as $pegawai) {
                    $pegawaiShiftKerja = new PegawaiShiftKerja();
                    $pegawaiShiftKerja->id_pegawai = $pegawai->id;
                    $pegawaiShiftKerja->id_shift_kerja = $id_shift_kerja;
                    $pegawaiShiftKerja->tanggal_berlaku = '2020-02-01';
                    //$pegawaiShiftKerja->save();

                    $this->stdout("Pegawai : ".$pegawai->nama.' - '.$nama_departemen.PHP_EOL, Console::FG_YELLOW);
                }
            } else {
                $this->stdout("Departemen tidak ada ".$nama_departemen.PHP_EOL, Console::FG_RED);
            }
        }

        $this->stdout($fileDir);
    }

    public function actionImportRekening()
    {
        $fileDir = 'files/import-rekening.xlsx';
        if (!file_exists($fileDir)) {
            $this->stdout('File Tidak Ditemukan'.PHP_EOL, Console::FG_RED);
        }

        $reader = IOFactory::load($fileDir);
        $reader->setActiveSheetIndex(0);
        $sheet = $reader->getActiveSheet();

        for ($i = 2; $i <= 1101; $i++) {
            $nama = trim($sheet->getCell("B$i")->getValue());
            $nip = trim($sheet->getCell("C$i")->getValue());
            $no_rekening = trim($sheet->getCell("D$i")->getValue());

            /* @var $pegawai Pegawai */
            $pegawai = Pegawai::find()->andWhere(['nip' => $nip])->one();
            if ($pegawai !== null) {
                $pegawai->nomor_rekening = $no_rekening;
                $pegawai->save();

                $this->stdout("Pegawai ".$pegawai->nama."berhasil ditambahkan data Rekening nya : $no_rekening ".PHP_EOL, Console::FG_YELLOW);
            } else {
                $this->stdout("Pegawai $nip tidak ditemukan".PHP_EOL, Console::FG_RED);
            }
        }

        $this->stdout($fileDir);
    }

    public function actionIndex()
    {
        $user = new User([
            'username' => 'admin',
            'password' => Yii::$app->security->generatePasswordHash('admin'),
            'email' => 'admin@example.com',
            'auth_key' => Yii::$app->security->generateRandomString(),
            'status' => User::STATUS_ACTIVE,
        ]);
        $user->save();
    }

    public function actionASetPegawaiShiftKerja($tahun='2019')
    {
        /* @var $pegawai Pegawai */
        /* @var $pegawaiShiftKerja PegawaiShiftKerja */

        $queryPegawai = Pegawai::find()->all();

        foreach ($queryPegawai as $pegawai) {
            $pegawaiShiftKerja = $pegawai->getPegawaiShiftKerjaBerlaku(date('Y-m-d'));
            if ($pegawaiShiftKerja !== null) {
                $this->stdout("Tanggal berlaku shif kerja berlaku adalah : $pegawaiShiftKerja->tanggal_berlaku akan kita rubah ke 2019-01-01".PHP_EOL,Console::FG_YELLOW);
                $pegawaiShiftKerja->tanggal_berlaku = $tahun.'-01-01';
                $pegawaiShiftKerja->save();
                $this->stdout("Tanggal berlaku shif kerja sudah dirubah".PHP_EOL,Console::FG_RED);
            }
        }
    }

    /**
     * Ekseskusi Saat Pertama kali jika UserInfo masih kosong
     */
    public function actionGenerateUserInfo()
    {
        $allPegawai = Pegawai::find()->all();
        $count = count($allPegawai);
        $i = 0;
        echo PHP_EOL;
        foreach ($allPegawai as $pegawai) {
            Console::clearLine();
            Console::moveCursorBackward(100);
            echo ++$i  . "/ $count";
            if ($pegawai->kode_presensi !== null && $pegawai->userInfo === null) {
                $stringNama = $pegawai->nama." - ".$pegawai->nip;
                $userinfo = new Userinfo([
                    'DelTag' => 0,
                    'name' => substr($stringNama, 0, 40),
                    'defaultdeptid' => 1,
                    'Privilege' => 0,
                    'AccGroup' => 1,
                    'TimeZones' => '0001000100000000',
                    'RegisterOT' => 1,
                    'badgenumber' => $pegawai->kode_presensi,
                ]);
                $userinfo->save(false);
                $this->stdout(" User Info Berhasil Dibuat ".PHP_EOL,Console::FG_RED);
            }
        }
    }

    public function actionKirimAllPegawaiKeMesin($SN_id)
    {
        $queryPegawai = Pegawai::find();
        $this->stdout("Jumlah Pegawai ".$queryPegawai->count()." Semua Data Akan Dikirim Ke Mesin Target".PHP_EOL,Console::FG_YELLOW);

        $nomer = 1;
        foreach ($queryPegawai->all() as $pegawai) {
            $this->stdout($nomer."/".$queryPegawai->count()." Pegawai Sedang Dikirim".PHP_EOL,Console::FG_YELLOW);

            /*
             * Run Internal
             * $pri = $pegawai->status_admin_mesin ? 14 : 0;
             * */
            $pri = 0;
            $cmd = new Devcmds([
                'CmdCommitTime' => date('Y-m-d H:i:s'),
                'SN_id' => $SN_id,
                'CmdContent' => "DATA USER PIN=$pegawai->kode_presensi\tName=$pegawai->nama - $pegawai->nip\tPri=$pri\tTZ=7\tGrp=1"
            ]);
            if ($cmd->save()) {
                /*
                 * Save Template DevCmds
                 * */
                if ($pegawai->userInfo !== null) {
                    /* @var $template Template */
                    foreach ($pegawai->userInfo->manyTemplate as $template) {
                        $cmd = new Devcmds([
                            'User_id' => 1,
                            'CmdCommitTime' => date('Y-m-d H:i:s'),
                            'SN_id' => $SN_id,
                            'CmdContent' => "DATA FP PIN=$pegawai->kode_presensi\tFID=$template->FingerID\tVALID=$template->Valid\tTMP=$template->Template"
                        ]);
                        $cmd->save(false);
                    }
                }

                $this->stdout("Pegawai Berhasil Dikirim $pegawai->nama".PHP_EOL,Console::FG_GREEN);
            }

            $nomer++;
        }
    }

    public function actionKirimPegawaiKeMesin($id_pegawai, $SN_id='A3AG193460078')
    {
        $pegawai = Pegawai::findOne($id_pegawai);

        if ($pegawai !== null) {
            $this->stdout("UserInfo Pegawai $pegawai->nama akan dikirim ke mesin $SN_id ".PHP_EOL,Console::FG_YELLOW);
            /*
             * Run Internal
             * $pri = $pegawai->status_admin_mesin ? 14 : 0;
             * */
            $pri = 14;
            $cmd = new Devcmds([
                'CmdCommitTime' => date('Y-m-d H:i:s'),
                'SN_id' => $SN_id,
                'CmdContent' => "DATA USER PIN=$pegawai->kode_presensi\tName=$pegawai->nama - $pegawai->nip\tPri=$pri\tTZ=7\tGrp=1"
            ]);

            if ($cmd->save()) {
                $this->stdout("UserInfo dalam proses pengiriman ".PHP_EOL,Console::FG_RED);
                /*
                 * Save Template DevCmds
                 * */
                if ($pegawai->userInfo !== null) {
                    /* @var $template Template */
                    foreach ($pegawai->userInfo->manyTemplate as $template) {
                        $cmd = new Devcmds([
                            'User_id' => 1,
                            'CmdCommitTime' => date('Y-m-d H:i:s'),
                            'SN_id' => $SN_id,
                            'CmdContent' => "DATA FP PIN=$pegawai->kode_presensi\tFID=$template->FingerID\tVALID=$template->Valid\tTMP=$template->Template"
                        ]);
                        $cmd->save(false);
                    }
                }
            }
        }
    }

    public function actionHapusPegawaiDariMesin($id_pegawai,$SN_id='A3AG191360732')
    {
        $pegawai = Pegawai::findOne($id_pegawai);

        if ($pegawai !== null) {
            $cmd = new Devcmds([
                'CmdCommitTime' => date('Y-m-d H:i:s'),
                'SN_id' => $SN_id,
                'CmdContent' => "DATA DEL_USER PIN=$pegawai->kode_presensi"
            ]);
            $cmd->save();
            $this->stdout("Successfully", Console::FG_GREEN);
        } else {
            $this->stdout("Unable to find pegawai", Console::FG_RED);
        }

    }

    public function actionUploadDataAgainSingle($sn='A3AG191360732')
    {
        $iclock = Iclock::findOne(['SN' => $sn]);
        if ($iclock !== null) {
            $cmd = new Devcmds([
                'User_id' => 1,
                'SN_id' => $iclock->SN,
                'CmdContent' => 'CHECK',
                'CmdCommitTime' => date('Y-m-d H:i:s'),
            ]);
            $cmd->save();
            $this->stdout("Successfully upload iclock $sn", Console::FG_GREEN);
        } else {
            $this->stdout("Unable to find iclock $sn", Console::FG_RED);
        }
        $this->stdout(PHP_EOL);
    }

    /**
     * @param null $id
     * @var $pegawai Pegawai
     */
    public function actionSetPegawaiPotonganRekap($id=null)
    {
        if ($id !== null) {
            $pegawai = Pegawai::findOne($id);
            $this->stdout("Pegawai Ditemukan Atas Nama : ".$pegawai->nama.PHP_EOL, Console::FG_GREEN);
            $manyPegawaiRekap = $pegawai->manyPegawaiRekapPayroll;
            $manyPegawaiPotonganBulan = $pegawai->manyPegawaiPotonganBulan;
            $manyPegawaiPotonganTarif = $pegawai->manyPegawaiPotonganTarifPtkp;

            foreach ($manyPegawaiRekap as $pegawaiRekap) {
                $pegawaiRekap->delete();
                $this->stdout("\t\tMenghapus Data Seluruh Rekap Pegawai : ".$pegawai->nama.PHP_EOL, Console::FG_RED);
            }
            foreach ($manyPegawaiPotonganBulan as $pegawaiPotonganBulan) {
                $pegawaiPotonganBulan->delete();
                $this->stdout("\t\tMenghapus Data Potongan Bulan : ".$pegawai->nama.PHP_EOL, Console::FG_RED);
            }
            foreach ($manyPegawaiPotonganTarif as $pegawaiPotonganTarif) {
                $pegawaiPotonganTarif->delete();
                $this->stdout("\t\tMenghapus Data Potongan Tarif PTKP : ".$pegawai->nama.PHP_EOL, Console::FG_RED);
            }
        } else {
            $manyPegawai = Pegawai::find()->all();

            foreach ($manyPegawai as $pegawai) {
                $this->stdout("Pegawai Ditemukan Atas Nama : ".$pegawai->nama.PHP_EOL, Console::FG_GREEN);
                $manyPegawaiRekap = $pegawai->manyPegawaiRekapPayroll;
                $manyPegawaiPotonganBulan = $pegawai->manyPegawaiPotonganBulan;
                $manyPegawaiPotonganTarif = $pegawai->manyPegawaiPotonganTarifPtkp;

                foreach ($manyPegawaiRekap as $pegawaiRekap) {
                    $pegawaiRekap->delete();
                    $this->stdout("\t\tMenghapus Data Seluruh Rekap Pegawai : ".$pegawai->nama.PHP_EOL, Console::FG_RED);
                }
                foreach ($manyPegawaiPotonganBulan as $pegawaiPotonganBulan) {
                    $pegawaiPotonganBulan->delete();
                    $this->stdout("\t\tMenghapus Data Potongan Bulan : ".$pegawai->nama.PHP_EOL, Console::FG_RED);
                }
                foreach ($manyPegawaiPotonganTarif as $pegawaiPotonganTarif) {
                    $pegawaiPotonganTarif->delete();
                    $this->stdout("\t\tMenghapus Data Potongan Tarif PTKP : ".$pegawai->nama.PHP_EOL, Console::FG_RED);
                }
            }
        }
    }

    public function actionSetKodePresensi()
    {
        $query = Pegawai::find()
        ->all();

        foreach ($query as $data) {
            $data->kode_presensi = $data->resetKodePresensi();
            $data->save();
        }
    }

    public function actionSetStatusTetap()
    {
        foreach (Pegawai::find()->all() as $pegawai) {
            if ($pegawai->status_tetap === null) {
                $pegawai->status_tetap = Pegawai::PEGAWAI_KOTRAK;
                $pegawai->save();
            }
        }
    }

    public function actionSetTanggalBerlakuShiftKerja()
    {
        $query = PegawaiShiftKerja::find()->andFilterWhere(['like','tanggal_berlaku','2020-01-01']);
        $jumlahData = $query->count();

        $count = 0;
        /* @var $data PegawaiShiftKerja */
        foreach ($query->all() as $data) {
            $data->tanggal_berlaku = '2019-12-01';
            $data->save(false);
            $this->stdout("$count/$jumlahData Pegawai : ".$data->pegawai->nama." - ".$data->shiftKerja->nama." - ".Helper::getTanggalSingkat($data->tanggal_berlaku).PHP_EOL, Console::FG_RED);
            $count++;
        }
    }
}
