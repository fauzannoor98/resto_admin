<?php
/**
 * Created by PhpStorm.
 * User: Fauzan
 * Date: 30/10/2019
 * Time: 19.46
 */

namespace app\commands;


use yii\console\Controller;

class ConnectionController extends Controller
{
    public function actionIndex()
    {
        $fp = fsockopen("www.google.com", 80, $errno, $errstr, 30);
        if (!$fp) {
            echo "$errstr ($errno)<br />\n";
        } else {
            $out = "GET / HTTP/1.1\r\n";
            $out .= "Host: www.example.com\r\n";
            $out .= "Connection: Close\r\n\r\n";
            fwrite($fp, $out);
            while (!feof($fp)) {
                echo fgets($fp, 128);
            }
            fclose($fp);
        }
    }
}
