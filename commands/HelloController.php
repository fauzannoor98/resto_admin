<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\components\Helper;
use yii\console\Controller;
use yii\helpers\Console;
use PhpOffice\PhpSpreadsheet\IOFactory;
use app\modules\iclock\models\Userinfo;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class HelloController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     */
    public function actionIndex()
    {
        $jam_mulai_absen = 24;
        $jam_selesai_absen = 27;

        $hitung = 0;

        for ($i = $jam_mulai_absen; $i < $jam_selesai_absen; $i++) {
            $hitung++;
            $this->stdout($i.PHP_EOL, Console::FG_RED);
        }

        $this->stdout("Total Lembur $hitung Jam".PHP_EOL, Console::FG_YELLOW);
    }
}
