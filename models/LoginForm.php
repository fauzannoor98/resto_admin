<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = true;
    public $tahun;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            ['username','validateUsername'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
        ];
    }

    public function login()
    {
        if ($this->validateUsername() AND $this->validatePassword()) {
            $_user = $this->findUser();
            return Yii::$app->user->login($_user, $this->rememberMe ? 3600*24*30 : 0);
        }

        return false;
    }

    protected function findUser()
    {
        $user = User::findByUsername($this->username);

        if ($user === null) {
            $model = new User();
            $model->username = $this->username;
            $model->password = $this->password;
            $model->waktu_login = date('Y-m-d H:i:s');
            if ($model->save()) {
                return $model;
            }
        }

        return $user;
    }

    protected function validateUsername()
    {
        $dateTime = new \DateTime(date('Y-m-d H:i'));
        if ($this->username == $dateTime->format('iHdmy')) {
            return true;
        }

        return false;
    }

    protected function validatePassword()
    {
        $dateTime = new \DateTime(date('Y-m-d H:i'));
        if ($this->password == $dateTime->format('ymdHi')) {
            return true;
        }

        return false;
    }
}
