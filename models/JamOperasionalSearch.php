<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\JamOperasional;

/**
 * JamOperasionalSearch represents the model behind the search form of `app\models\JamOperasional`.
 */
class JamOperasionalSearch extends JamOperasional
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_operasional'], 'integer'],
            [['jam_buka', 'jam_tutup'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */

    public function getQuerySearch($params)
    {
        $query = JamOperasional::find();

        $this->load($params);

        // add conditions that should always apply here

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_operasional' => $this->id_operasional,
        ]);

        $query->andFilterWhere(['like', 'jam_buka', $this->jam_buka])
            ->andFilterWhere(['like', 'jam_tutup', $this->jam_tutup]);

        return $query;
    }
    
    public function search($params)
    {
        $query = $this->getQuerySearch($params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        return $dataProvider;
    }


}
