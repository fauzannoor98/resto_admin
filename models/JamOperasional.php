<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jam_operasional".
 *
 * @property int $id
 * @property int $id_operasional
 * @property string $jam_buka
 * @property string $jam_tutup
 */
class JamOperasional extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jam_operasional';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_operasional', 'jam_buka', 'jam_tutup'], 'required'],
            [['id_operasional'], 'integer'],
            [['jam_buka', 'jam_tutup'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_operasional' => 'Id Operasional',
            'jam_buka' => 'Jam Buka',
            'jam_tutup' => 'Jam Tutup',
        ];
    }
}
