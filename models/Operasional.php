<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "operasional".
 *
 * @property int $id
 * @property int $id_hari
 * @property string $tanggal
 */
class Operasional extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'operasional';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['id_hari','unique','message' => '{attribute} Sudah Di Inputkan, Silahkan Pilih Hari Yang Lain'],
            [['id_hari'], 'required'],
            [['id_hari'], 'integer'],
            [['tanggal'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_hari' => 'Hari',
            'tanggal' => 'Tanggal Input',
        ];
    }

    public function getManyJamOperasional()
    {
        return $this->hasMany(JamOperasional::class,['id_operasional' => 'id']);
    }
}
