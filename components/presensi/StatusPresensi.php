<?php
/**
 * Created by PhpStorm.
 * User: Fauzan
 * Date: 11/10/2019
 * Time: 15.01
 */

namespace app\components\presensi;


class StatusPresensi
{
    /**
     * Tidak ada Presensi.
     */
    const TANPA_KETERANGAN = 0;
    /**
     * Presensi ada dan tepat waktu.
     */
    const OK = 1;
    /**
     * Presensi ada dalam jangkauan jam perhitungan tetapi di luar jam checkin.
     */
    const TERLAMBAT = 2;
    /**
     * Itungan Lembur
     * */
    const LEMBUR = 3;
    /**
     * Hari libur.
     */
    const LIBUR = 4;
    /**
     * Ada izin presensi
     */
    const KETIDAKHADIRAN_PANJANG = 5;
    /**
     * Ada izin presensi jam kerja.
     */
    const KETIDAKHADIRAN_JAM_KERJA = 6;
    /**
     * Presensi ada di masa depan dan biasanya belum perlu dihitung.
     */
    const MASA_DEPAN = 7;

    /**
     * @var array keterangan dari status codes.
     */
    public static $keterangan = [
        self::TANPA_KETERANGAN => 'Tanpa Keterangan',
        self::OK => 'Tepat Waktu',
        self::TERLAMBAT => 'Tidak Tepat Waktu',
        self::LEMBUR => 'Lembur',
        self::LIBUR => 'Hari Libur',
        self::MASA_DEPAN => '',
    ];

    /**
     * @param int $statusCode salah satu dari constant.
     * @param string $default display default jika keterangan tidak ditemukan.
     * @return string keterangan text dari status code.
     */
    public static function getKeterangan($statusCode, $default = 'Keterangan tidak diketahui')
    {
        return isset(static::$keterangan[$statusCode]) ? static::$keterangan[$statusCode] : $default;
    }

    public static function getList()
    {
        return [
            self::TANPA_KETERANGAN => 'Tanpa Keterangan',
            self::OK => 'Tepat Waktu',
            self::TERLAMBAT => 'Tidak Tepat Waktu',
            self::LEMBUR => 'Lembur',
            self::LIBUR => 'Hari Libur',
            self::MASA_DEPAN => '',
        ];
    }
}
