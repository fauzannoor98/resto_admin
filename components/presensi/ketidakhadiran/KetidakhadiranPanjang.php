<?php
/**
 * Created by PhpStorm.
 * User: Fauzan
 * Date: 11/10/2019
 * Time: 23.30
 */

namespace app\components\presensi\ketidakhadiran;


use app\models\JamKerja;
use yii2mod\query\ArrayQuery;

class KetidakhadiranPanjang extends BaseKetidakhadiran
{
    public function setKetidakhadiran()
    {
        if (empty($this->ketidakhadiran)) {
            $this->ketidakhadiran = $this->getPegawai()->getManyKetidakhadiranPanjangBulan($this->getDate());
        }
    }

    public function getHasKetidakhadiran(\DateTime $dateTime, JamKerja $jamKerja = null)
    {
        if (!empty($this->ketidakhadiran)) {
            $query = new ArrayQuery();
            $tanggal_awal = $dateTime->format('Y-m-d');
            $tanggal_akhir = $dateTime->format('Y-m-d');
            $query->from($this->ketidakhadiran)
                ->andWhere(['<=', 'tanggal_mulai', $tanggal_awal])
                ->andWhere(['>=', 'tanggal_selesai', $tanggal_akhir]);

            return $query->one();
        }

        return false;
    }
}
