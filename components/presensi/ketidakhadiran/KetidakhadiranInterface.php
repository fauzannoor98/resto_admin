<?php
/**
 * Created by PhpStorm.
 * User: Fauzan
 * Date: 11/10/2019
 * Time: 23.32
 */

namespace app\components\presensi\ketidakhadiran;


interface KetidakhadiranInterface
{
    public function setKetidakhadiran();
}
