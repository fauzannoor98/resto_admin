<?php
/**
 * Created by PhpStorm.
 * User: Fauzan
 * Date: 11/10/2019
 * Time: 23.31
 */

namespace app\components\presensi\ketidakhadiran;


use app\components\presensi\PresensiBulan;
use app\models\JamKerja;
use yii\base\BaseObject;

abstract class BaseKetidakhadiran extends BaseObject implements KetidakhadiranInterface
{
    /**
     * @param \DateTime $dateTime
     * @param JamKerja $jamKerja
     * @return mixed
     */
    abstract public function getHasKetidakhadiran(\DateTime $dateTime, JamKerja $jamKerja);
    /**
     * @var \app\models\KetidakhadiranPanjang[]|\app\models\KetidakhadiranJamKerja[]
     */
    public $ketidakhadiran = [];
    /**
     * @var PresensiBulan
     */
    private $_presensiBulan;

    public function init()
    {
       $this->setKetidakhadiran();
    }

    public function __construct(PresensiBulan $presensiBulan, array $config = [])
    {
        $this->_presensiBulan = $presensiBulan;
        parent::__construct($config);
    }

    /**
     * @return PresensiBulan
     */
    public function getPresensiBulan(): PresensiBulan
    {
        return $this->_presensiBulan;
    }

    /**
     * @return \app\models\Pegawai
     */
    public function getPegawai()
    {
        return $this->getPresensiBulan()->pegawai;
    }

    public function getDate()
    {
        return $this->getPresensiBulan()->date;
    }
}
