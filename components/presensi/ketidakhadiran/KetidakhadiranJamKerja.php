<?php
/**
 * Created by PhpStorm.
 * User: Fauzan
 * Date: 12/10/2019
 * Time: 15.19
 */

namespace app\components\presensi\ketidakhadiran;


use app\models\JamKerja;
use yii2mod\query\ArrayQuery;

class KetidakhadiranJamKerja extends BaseKetidakhadiran
{
    /**
     * @inheritdoc
     */
    public function setKetidakhadiran()
    {
        if ($this->ketidakhadiran === []) {
            $this->ketidakhadiran = $this->getPegawai()->getManyKetidakhadiranJamKerjaBulan($this->date);
        }
    }

    public function getHasKetidakhadiran(\DateTime $dateTime, JamKerja $jamKerja)
    {
        if ($this->ketidakhadiran !== null) {
            $query = new ArrayQuery();
            $query->from($this->ketidakhadiran)
                ->andWhere([
                    'tanggal' => $dateTime->format('Y-m-d'),
                    'id_jam_kerja' => $jamKerja->id,
                ]);
            return $query->one();
        }
        return false;
    }
}
