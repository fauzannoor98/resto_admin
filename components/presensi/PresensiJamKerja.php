<?php
/**
 * Created by PhpStorm.
 * User: Fauzan
 * Date: 11/10/2019
 * Time: 11.58
 */

namespace app\components\presensi;


use app\models\JamKerja;
use app\models\KetidakhadiranJamKerja;
use app\modules\iclock\models\Checkinout;
use yii2mod\query\ArrayQuery;

class PresensiJamKerja extends BasePresensi
{
    /**
     * @var \DateTime
     */
    protected $_date;
    /**
     * @var JamKerja
     */
    protected $_jamKerja;
    /**
     * @var PresensiHari
     */
    protected $_presensiHari;

    /**
     * @var int
     * @see StatusPresensi
     */
    private $_statusPresensi;

    /**
     * @var Checkinout[]
     */
    private $_checkinout;

    private $_checkinoutOrderByOne;

    /**
     * @var null|int Menit telat
     */
    private $_menitTelat;

    /**
     * @inheritdoc
     * @param PresensiHari $presensiHari
     * @param JamKerja $jamKerja
     * @param array $config
     */
    public function __construct(PresensiHari $presensiHari, JamKerja $jamKerja, array $config = [])
    {
        $this->_presensiHari =& $presensiHari;
        $this->_jamKerja = $jamKerja;
        parent::__construct($config);
    }

    /**
     * @inheritDoc
     */
    public function init()
    {
        $this->setCheckinout();
        $this->setCheckinoutOrderBy();
        //$this->setStatusPresensi();
        //$this->setMenitTelat();
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->presensiHari->date;
    }

    /**
     * @return PresensiHari
     */
    public function getPresensiHari()
    {
        return $this->_presensiHari;
    }

    /**
     * @return PresensiBulan
     */
    public function getPresensiBulan()
    {
        return $this->presensiHari->presensiBulan;
    }

    /**
     * @return Pegawai
     */
    public function getPegawai()
    {
        return $this->presensiHari->pegawai;
    }

    /**
     * @return JamKerja
     */
    public function getJamKerja()
    {
        return $this->_jamKerja;
    }

    /**
     * Mengambil checkinout pada jangkauan hitung (tetapi belum tentu tepat waktu).
     * @see getResultCheckinout()
     * @return Checkinout[]
     */
    public function getCheckinout()
    {
        if($this->_checkinout===null) {
            $this->setCheckinout();
        }

        return $this->_checkinout;

    }

    /**
     * Setter checkinout
     * @see getCheckinout()
     */
    private function setCheckinout()
    {
        $presensiBulan = $this->getPresensiBulan();

        $query = new ArrayQuery();
        $query->from($presensiBulan->getCheckinoutBulan());
        $query->where([
            'between',
            'checktime',
            $this->getDateMulaiHitung(),
            $this->getDateSelesaiHitung(),
        ]);

        $array = $query->all();
        $this->_checkinout = $array;
    }

    private function setCheckinoutOrderBy()
    {
        $presensiBulan = $this->getPresensiBulan();

        $query = new ArrayQuery();
        $query->from($presensiBulan->getCheckinoutBulan());
        $query->where([
            'between',
            'checktime',
            $this->getDateMulaiHitung(),
            $this->getDateSelesaiHitung(),
        ]);

        $array = $query->orderBy(['checktime' => SORT_ASC])->one();
        $this->_checkinoutOrderByOne = $array;
    }

    /**
     * @return Checkinout
     */
    public function getCheckinoutOrderBy()
    {
        if (empty($this->_checkinoutOrderByOne)) {
            $this->setCheckinoutOrderBy();
        }

        return $this->_checkinoutOrderByOne;
    }

    /**
     * @return string Mengambil string checkinout yang dilakukan pegawai.
     */
    public function getStringCheckinout()
    {
        $checkinout = $this->getCheckinout();

        return implode(',',
            array_map(function (Checkinout $checkinout) {
                return $checkinout->getTime();
            },
                $checkinout
            )
        );
    }

    public function getHasKetidakhadiranJamKerja($setuju = false)
    {
        return (bool) $this->getKetidakhadiranJamKerja($setuju);
    }

    /**
     * @param bool $setuju
     * @return KetidakhadiranJamKerja
     */
    public function getKetidakhadiranJamKerja($setuju = false)
    {
        $ketidakhadiranJamkerja = $this->getPresensiBulan()->getHasKetidakhadiranJamKerja($this->date, $this->jamKerja);
        if ($setuju) {
            if ($ketidakhadiranJamkerja) {
                return $ketidakhadiranJamkerja->getIsDisetujui();
            }

            return false;
        }
        return $ketidakhadiranJamkerja;
    }

    /**
     * @return string Keterangan Presensi
     */
    public function getKeteranganPresensi()
    {
        if ($this->getPresensiHari()->getHasKetidakhadiranPanjang(true)) {
            return "";
        } elseif ($this->getHasKetidakhadiranJamKerja()) {
            return $this->getKetidakhadiranJamKerja()->ketidakhadiranJenis->nama;
        } elseif ($this->getPresensiHari()->hasHariLibur() && @$this->getPresensiHari()->getPegawaiShiftKerja()->shiftKerja->getIsLiburNasional()) {
            return 'Hari Libur';
        } else {
            return StatusPresensi::getKeterangan($this->getStatusPresensi());
        }
    }

    /**
     * @return int Status presensi berdasarkan kode
     * @see StatusPresensi
     */
    public function getStatusPresensi()
    {
        if ($this->_statusPresensi === null) {
            $this->setStatusPresensi();
        }

        return $this->_statusPresensi;
    }

    /**
     * Set status presensi.
     */
    public function setStatusPresensi()
    {
        /*
         * Jika Masa Depan
         * */
        if ($this->getDateMulaiHitung() > date('Y-m-d H:i:s')) {
            $this->_statusPresensi = StatusPresensi::MASA_DEPAN;
            return;
        }

        if ($this->getJamKerja()->isJamKerjaLembur) {
            $this->_statusPresensi = StatusPresensi::LEMBUR;
            return;
        }

        if ($this->isTepatWaktu() AND !$this->getJamKerja()->isJamKerjaLembur) {
            $this->_statusPresensi = StatusPresensi::OK;
            return;
        } else {
            $this->_statusPresensi = StatusPresensi::TANPA_KETERANGAN;
            return;
        }
    }

    /**
     * Mengecek apakah waktu presensi tepat waktu atau tidak, berdasarkan data dari checkinout pegawai terkait.
     * @return Checkinout|false apakah presensi tepat waktu atau tidak
     */
    public function isTepatWaktu()
    {
        foreach ($this->getCheckinout() as $checkinout) {
            if ($checkinout->checktime >= $this->getDateMulaiHitung() &&
                $checkinout->checktime <= $this->getDateSelesaiHitung()) {
                return true;
            }
            // Cek apakah masuk toleransi
            /*if ($checkinout->checktime >= $this->getDateMulaiHitung() &&
                $checkinout->checktime <= $this->getDateSelesaiHitung()) {
                return true;
            }*/
        }
        return false;
    }

    /**
     * @return string
     */
    public function getDateMulaiHitung()
    {
        return $this->getJamKerja()->getTanggalJamMinimalAbsen($this->getTanggal());
    }

    /**
     * @return string
     */
    public function getDateSelesaiHitung()
    {
        return $this->getJamKerja()->getTanggalJamMaksimalAbsen($this->getTanggal());
    }

    public function getClassStatusPresensiTr()
    {
        if ($this->getJamKerja()->isJamKerjaLembur) {
            return 'bg-warning';
        }

        return '';
    }

}
