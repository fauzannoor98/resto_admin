<?php
/**
 * Created by PhpStorm.
 * User: Fauzan
 * Date: 11/10/2019
 * Time: 00.03
 */

namespace app\components\presensi;

abstract class BasePresensi extends \yii\base\BaseObject
{
    /**
     * @return \DateTime
     */
    abstract public function getDate();

    /**
     * @return string
     */
    public function getTanggal()
    {
        return $this->getDate()->format("Y-m-d");
    }

    /**
     * @return string
     */
    public function getHari()
    {
        return \app\components\Helper::getNamaHariFromTanggal($this->tanggal);
    }
}
