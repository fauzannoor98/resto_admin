<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Operasional */

$this->title = "Tambah Operasional";
$this->params['breadcrumbs'][] = ['label' => 'Operasionals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="operasional-create">

    <?= $this->render('_form', [
        'model' => $model,
        'referrer'=> $referrer
    ]) ?>

</div>
