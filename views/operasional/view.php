<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Operasional */

$this->title = "Detail Operasional";
$this->params['breadcrumbs'][] = ['label' => 'Operasional', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="operasional-view box box-primary">

    <div class="box-header with-border">
        <h3 class="box-title">Detail Operasional</h3>
    </div>

    <div class="box-body">

    <?= DetailView::widget([
        'model' => $model,
        'template' => '<tr><th width="180px" style="text-align:right">{label}</th><td>{value}</td></tr>',
        'attributes' => [
            [
                'attribute' => 'id_hari',
                'format' => 'raw',
                'value' => \app\components\Helper::getHari($model->id_hari),
            ],
            [
                'attribute' => 'tanggal',
                'format' => 'raw',
                'value' => \app\components\Helper::getWaktuWIB($model->tanggal),
            ],
        ],
    ]) ?>

    </div>

    <div class="box-footer">
        <?= Html::a('<i class="fa fa-pencil"></i> Sunting Operasional', ['update', 'id' => $model->id], ['class' => 'btn btn-success btn-flat']) ?>
        <?= Html::a('<i class="fa fa-list"></i> Daftar Operasional', ['index'], ['class' => 'btn btn-warning btn-flat']) ?>
    </div>
</div>


<?= $this->render('_list-jam-operasional',['model' => $model]) ?>