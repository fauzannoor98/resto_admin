<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Operasional */

$this->title = "Sunting Operasional";
$this->params['breadcrumbs'][] = ['label' => 'Operasionals', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Sunting';
?>
<div class="operasional-update">

    <?= $this->render('_form', [
        'model' => $model,
        'referrer'=> $referrer
    ]) ?>

</div>
