<?php 

use yii\helpers\Html;
use yii\widgets\DetailView;

?>

<div class="box">
    <div class="box-header with-border">
        <?= Html::a('<i class="fa fa-plus"></i> Tambah Jam Operasional', ['/jam-operasional/create','id_operasional' => $model->id], ['class' => 'btn btn-primary btn-flat']); ?>
    </div>

    <div class="box-body">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th style="text-align: center; width: 55px">No</th>
                    <th style="text-align: center">Jam Buka</th>
                    <th style="text-align: center">Jam Tutup</th>
                    <th style="text-align: center; width: 75px"></th>
                </tr>
            </thead>
            <tbody>
                <?php $no = 1; ?>
                <?php /* @var $jamOperasional app\models\JamOperasional */ ?>
                <?php foreach ($model->manyJamOperasional as $jamOperasional) { ?>
                    <tr>
                        <td style="text-align: center;">
                            <?= $no ?>
                        </td>
                        <td style="text-align: center;">
                            <?= $jamOperasional->jam_buka ?>
                        </td>
                        <td style="text-align: center;">
                            <?= $jamOperasional->jam_tutup ?>
                        </td>
                        <td style="text-align: center;">
                            <?= Html::a('<i class="fa fa-pencil"></i>', ['jam-operasional/update','id' => $jamOperasional->id], ['option' => 'value']); ?>
                            <?= Html::a('<i class="fa fa-trash"></i>', ['jam-operasional/delete','id' => $jamOperasional->id], ['data-method' => 'post','data-confirm' => 'Apakah anda yakin akan menghapus data berikut?']); ?>
                        </td>
                    </tr>
                <?php $no++; } ?>
            </tbody>
        </table>
    </div>
</div>