<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\OperasionalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Daftar Operasional';
$this->params['breadcrumbs'][] = $this->title;
?>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<div class="operasional-index box box-primary">

    <div class="box-header with-border">
        <?= Html::a('<i class="fa fa-plus"></i> Tambah Operasional', ['create'], ['class' => 'btn btn-primary btn-flat']) ?>

    </div>

    <div class="box-body">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'header' => 'No',
                'headerOptions' => ['style' => 'text-align:center;  width: 50px'],
                'contentOptions' => ['style' => 'text-align:center']
            ],
            [
                'class' => 'kartik\grid\ExpandRowColumn',
                'value' => function ($model, $key, $index, $column) {
                    return GridView::ROW_COLLAPSED;
                },
                'detail' => function ($model, $key, $index, $column) {
                    return $this->render('_list-jam-operasional', ['model' => $model]);
                },
                'headerOptions' => ['class' => 'kartik-sheet-style'],
                'expandOneOnly' => true,
            ],
            [
                'attribute' => 'id_hari',
                'format' => 'raw',
                'value' => function($data) {
                    return \app\components\Helper::getHari($data->id_hari);
                },
                'headerOptions' => ['style' => 'text-align:center;'],
                'contentOptions' => ['style' => 'text-align:center;'],
            ],
            [
                'header' => 'Tanggal Input',
                'format' => 'raw',
                'value' => function($data) {
                    return \app\components\Helper::getWaktuWIB($data->tanggal);
                },
                'headerOptions' => ['style' => 'text-align:center; width: 155px'],
                'contentOptions' => ['style' => 'text-align:center;'],
            ],
            [
                'header' => 'Jam Operasional',
                'format' => 'raw',
                'value' => function($data) {
                    return '<span class="label label-success" data-toggle="tooltip" title="Jumlah Jam Operasional">'.count($data->manyJamOperasional).'</span>';
                },
                'headerOptions' => ['style' => 'text-align:center; width: 90px'],
                'contentOptions' => ['style' => 'text-align:center;'],
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'text-align:center;width:80px']
            ],
        ],
    ]); ?>
    </div>
</div>
