<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\JamOperasional */

$this->title = "Tambah Jam Operasional";
$this->params['breadcrumbs'][] = ['label' => 'Jam Operasionals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jam-operasional-create">

    <?= $this->render('_form', [
        'model' => $model,
        'referrer'=> $referrer
    ]) ?>

</div>
