<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\JamOperasional */

$this->title = "Sunting Jam Operasional";
$this->params['breadcrumbs'][] = ['label' => 'Jam Operasionals', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Sunting';
?>
<div class="jam-operasional-update">

    <?= $this->render('_form', [
        'model' => $model,
        'referrer'=> $referrer
    ]) ?>

</div>
