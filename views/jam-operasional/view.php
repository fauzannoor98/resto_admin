<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\JamOperasional */

$this->title = "Detail Jam Operasional";
$this->params['breadcrumbs'][] = ['label' => 'Jam Operasional', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jam-operasional-view box box-primary">

    <div class="box-header with-border">
        <h3 class="box-title">Detail Jam Operasional</h3>
    </div>

    <div class="box-body">

    <?= DetailView::widget([
        'model' => $model,
        'template' => '<tr><th width="180px" style="text-align:right">{label}</th><td>{value}</td></tr>',
        'attributes' => [
            [
                'attribute' => 'id',
                'format' => 'raw',
                'value' => $model->id,
            ],
            [
                'attribute' => 'id_operasional',
                'format' => 'raw',
                'value' => $model->id_operasional,
            ],
            [
                'attribute' => 'jam_buka',
                'format' => 'raw',
                'value' => $model->jam_buka,
            ],
            [
                'attribute' => 'jam_tutup',
                'format' => 'raw',
                'value' => $model->jam_tutup,
            ],
        ],
    ]) ?>

    </div>

    <div class="box-footer">
        <?= Html::a('<i class="fa fa-pencil"></i> Sunting Jam Operasional', ['update', 'id' => $model->id], ['class' => 'btn btn-success btn-flat']) ?>
        <?= Html::a('<i class="fa fa-list"></i> Daftar Jam Operasional', ['index'], ['class' => 'btn btn-warning btn-flat']) ?>
    </div>

</div>
