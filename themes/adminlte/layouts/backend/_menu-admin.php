<?php

use app\models\UserRole;

?>

<?= dmstr\widgets\Menu::widget(
    [
        'options' => ['class' => 'sidebar-menu', 'data-widget' => 'tree'],
        'items' => [

            ['label' => 'Dashboard', 'icon' => 'dashboard', 'url' => ['/admin/index']],
            ['label' => 'DAFTAR GAJI PEGAWAI','options' => ['class' => 'header']],
            ['label' => 'Pegawai Payroll', 'icon' => 'users', 'url' => ['/pegawai/index-payroll'],],
            ['label' => 'Pegawai Operasional', 'icon' => 'users', 'url' => ['/pegawai-operasional/index'],],
            ['label' => 'Potongan', 'icon' => 'cut', 'url' => ['/potongan/index']],

            ['label' => 'ABSENSI PEGAWAI','options' => ['class' => 'header']],
            [
                'label' => 'Kepegawaian',
                'icon' => 'user',
                'url' => '#',
                'items' => [
                    ['label' => 'Pegawai Absensi', 'icon' => 'circle-o', 'url' => ['/pegawai/index-absensi'],],
                    ['label' => 'Pegawai Mutasi', 'icon' => 'circle-o', 'url' => ['/pegawai/index-mutasi-pegawai'],],
                    ['label' => 'Pegawai Shift Kerja', 'icon' => 'circle-o', 'url' => ['/pegawai/index-shift-kerja'],],
                    ['label' => 'Absensi Manual', 'icon' => 'circle-o', 'url' => ['/absensi-manual/index']],

                ]
            ],
            ['label' => 'Mesin Absen', 'icon' => 'television', 'url' => ['/departemen/index-mesin']],
            ['label' => 'Ketidakhadiran Hari Kerja', 'icon' => 'book', 'url' => ['/ketidakhadiran-panjang/index']],
            ['label' => 'Ketidakhadiran Jam Kerja', 'icon' => 'clock-o', 'url' => ['/ketidakhadiran-jam-kerja/index']],
            ['label' => 'Shift Kerja', 'icon' => 'refresh', 'url' => ['/shift-kerja/index']],
            ['label' => 'Hari Libur', 'icon' => 'calendar-o', 'url' => ['/hari-libur/index']],

            ['label' => 'ABSENSI MOBILE','options' => ['class' => 'header'],'visible' => Yii::$app->params['fiturPegawaiLogin']],
            ['label' => 'Daftar Absensi', 'icon' => 'mobile-phone', 'url' => ['/absensiMobile/absensi/index'],'visible' => Yii::$app->params['fiturPegawaiLogin']],
            ['label' => 'Refrensi API', 'icon' => 'code', 'url' => ['/api/default/index'],'visible' => Yii::$app->params['fiturPegawaiLogin']],

            ['label' => 'ADMS SERVER','options' => ['class' => 'header']],
            [
                'label' => 'ADMS',
                'icon' => 'circle-o',
                'url' => '#',
                'items' => [
                    ['label' => 'Automatic Data Master', 'icon' => 'circle-o', 'url' => 'localhost:8082','template' => '<a href="{url}" target="_blank">{icon} {label}</a>'],
                    ['label' => 'Departments', 'icon' => 'circle-o', 'url' => ['/iclock/departments/index']],
                    ['label' => 'DevCmds', 'icon' => 'circle-o', 'url' => ['/iclock/devcmds/index']],
                    ['label' => 'IClock', 'icon' => 'circle-o', 'url' => ['/iclock/iclock/index']],
                    ['label' => 'Template', 'icon' => 'circle-o', 'url' => ['/iclock/template/index']],
                    ['label' => 'User Info', 'icon' => 'circle-o', 'url' => ['/iclock/userinfo/index']],
                    ['label' => 'Checkinout', 'icon' => 'circle-o', 'url' => ['/iclock/checkinout/index']],
                ]
            ],

            ['label' => 'DATA MASTER','options' => ['class' => 'header']],
            ['label' => 'Rekapan Bulan', 'icon' => 'folder-open', 'url' => ['/rekapan-bulan/index']],

            ['label' => 'Pegawai', 'icon' => 'users', 'url' => ['/pegawai/index'],'template' => '<a href="{url}">{icon} {label} <span class="pull-right-container"><span class="label label-primary pull-right">'.\app\models\Pegawai::getCount().'</span></span></a>'],
            ['label' => 'Mutasi Pegawai', 'icon' => 'refresh', 'url' => ['/pegawai-departemen/index']],
            ['label' => 'Departemen', 'icon' => 'building', 'url' => ['/departemen/index']],
            ['label' => 'Depertemen Bagian', 'icon' => 'thumb-tack', 'url' => ['/departemen-bagian/index']],
            ['label' => 'Depertemen Jabatan', 'icon' => 'star', 'url' => ['/departemen-jabatan/index']],
            ['label' => 'Operasional', 'icon' => 'gears', 'url' => ['/operasional/index']],
            ['label' => 'Kabupaten', 'icon' => 'bank', 'url' => ['/kabupaten/index']],

            ['label' => 'SISTEM','options' => ['class' => 'header']],
            [
                'label' => 'User',
                'icon' => 'user',
                'url' => '#',
                'items' => [
                    ['label' => 'Super Admin', 'icon' => 'circle-o', 'url' => ['/user/index','id_user_role' => UserRole::ADMIN],],
                    ['label' => 'Atasan', 'icon' => 'circle-o', 'url' => ['/user/index','id_user_role' => UserRole::ATASAN],],
                    ['label' => 'Admin Absensi', 'icon' => 'circle-o', 'url' => ['/user/index','id_user_role' => UserRole::ADMIN_ABSENSI],],
                    ['label' => 'Pegawai', 'icon' => 'circle-o', 'url' => ['/user/index','id_user_role' => UserRole::PEGAWAI],'visible' => Yii::$app->params['fiturPegawaiLogin']],
                ]
            ],
            [
                'label' => 'Panduan',
                'icon' => 'download',
                'url' => '#',
                'items' => [
                    ['label' => 'Panduan Admin', 'icon' => 'download', 'url' => Yii::getAlias('@web'.'/panduan/admin-absensi.pdf'),'template' => '<a href="{url}" target="_blank">{icon} {label}</a>'],
                    ['label' => 'Panduan Admin Absensi', 'icon' => 'download', 'url' => Yii::getAlias('@web'.'/panduan/super-admin.pdf'),'template' => '<a href="{url}" target="_blank">{icon} {label}</a>'],
                ]
            ],
            ['label' => 'Logout','icon' => 'lock', 'url' => ['/site/logout'], 'template' => '<a href="{url}" data-method="post">{icon} {label}</a>', 'visible' => !Yii::$app->user->isGuest],
        ]
    ]
) ?>
